package cmu.fabrice.sit2long;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private GoogleApiClient apiClient;
    private TextView counterTextView;
    private TextView statusTextView;
    private Button startButton;
    private Button stopButton;
    private Button pauseButton;
    private Button resetButton;
    private EditText minutesEditText;
    private boolean running = false;
    private CountDownTimer countDownTimer;
    private long leftMilliSeconds = 1L;
    private long enteredMilliSeconds = 1L;
    private int enteredMinutes;
    private BroadcastReceiver broadcastReceiver;
    private static final String DETECTED_ACTIVITY = "detectedActivity";
    private ArrayList receivedActivities = new ArrayList();
    private static final String CHANNEL_ID = "This channel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        counterTextView = findViewById(R.id.countdownText);
        statusTextView = findViewById(R.id.statusText);
        startButton = findViewById(R.id.startbtn);
        stopButton = findViewById(R.id.stopbtn);
        pauseButton = findViewById(R.id.pausebtn);
        resetButton = findViewById(R.id.resetbtn);
        minutesEditText = findViewById(R.id.mintxt);


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runTimer();
                startService();
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopTimer();
                stopService();
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseTimer();
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
            }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(DETECTED_ACTIVITY)) {
                    int type = intent.getIntExtra("Type", -1);
                    int confidence = intent.getIntExtra("Confidence", 0);
                    displayState(type, confidence);
                    receivedActivities.add(type);
                }
            }
        };

        startService();

        apiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(MainActivity.this)
                .addOnConnectionFailedListener(MainActivity.this).build();
        apiClient.connect();
    }


    private void runTimer() {
        if (!minutesEditText.getText().toString().equals("") && minutesEditText.getText().toString().length() > 0) {
            enteredMinutes = Integer.parseInt(minutesEditText.getText().toString());
            leftMilliSeconds = enteredMinutes * 60 * 1000;
            enteredMilliSeconds = leftMilliSeconds;
            countDownTimer = new CountDownTimer(leftMilliSeconds, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    leftMilliSeconds = millisUntilFinished;
                    updateTextView();
                }

                @Override
                public void onFinish() {
                            notification("Activity likely happening ", "You have been sitting for too long");
                }
            }.start();
            running = true;

        } else {
            Toast.makeText(MainActivity.this, "Please provide a number of minutes", Toast.LENGTH_SHORT).show();
        }

    }

    private void stopTimer() {
        if (running) {
            countDownTimer.cancel();
            leftMilliSeconds = 0;
            running = false;
            updateTextView();
        } else {
            Toast.makeText(MainActivity.this, "The countdown needs to be running", Toast.LENGTH_SHORT).show();
        }
    }

    private void pauseTimer() {
        if (running) {
            countDownTimer.cancel();
            running = false;
        } else {
            Toast.makeText(MainActivity.this, "The countdown needs to be running", Toast.LENGTH_SHORT).show();
        }
    }

    private void resetTimer() {
        if (!minutesEditText.getText().toString().equals("") && minutesEditText.getText().toString().length() > 0) {
            if (running) {
                countDownTimer.cancel();
                leftMilliSeconds = enteredMilliSeconds;
                running = false;
                updateTextView();
            } else if (!running) {
                countDownTimer.cancel();
                leftMilliSeconds = enteredMilliSeconds;
                updateTextView();
            } else {
                Toast.makeText(MainActivity.this, "The countdown hasn't started", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MainActivity.this, "You need to provide a number of minutes", Toast.LENGTH_SHORT).show();
        }

    }

    private void updateTextView() {
        int leftMinutes = (int) (leftMilliSeconds / 1000 / 60);
        int leftSeconds = (int) (leftMilliSeconds / 1000 % 60);
        String leftTimeFormat = String.format(Locale.getDefault(), "%02d:%02d", leftMinutes, leftSeconds);
        counterTextView.setText(leftTimeFormat);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        ActivityRecognitionClient client = new ActivityRecognitionClient(MainActivity.this);
        Intent intent = new Intent(MainActivity.this, RecognitionActivity.class);
        PendingIntent pendingIntent = PendingIntent.getService(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        client.requestActivityUpdates(5000, pendingIntent);
    }

    public void displayState(int type, int confidence) {
        Log.d("TYPE", "" + type);
        switch (type) {
            case DetectedActivity.IN_VEHICLE: {
                if(confidence > 40){
                    statusTextView.setText("IN VEHICLE");
                    break;
                }
            }
            case DetectedActivity.ON_BICYCLE: {
                if(confidence > 40){
                    statusTextView.setText("ON BICYCLE");
                    break;
                }
            }
            case DetectedActivity.ON_FOOT: {
                if(confidence > 40){
                    statusTextView.setText("ON FOOT");
                    break;
                }
            }
            case DetectedActivity.RUNNING: {
                if(confidence > 40){
                    statusTextView.setText("RUNNING");
                    break;
                }
            }
            case DetectedActivity.STILL: {
                if(confidence > 40){
                    statusTextView.setText("STILL");
                    break;
                };
            }
            case DetectedActivity.TILTING: {
                if(confidence > 40){
                    statusTextView.setText("TILTING");
                    break;
                }
            }
            case DetectedActivity.WALKING: {
                if(confidence > 40){
                    statusTextView.setText("WALKING");
                    break;
                }
            }
            case DetectedActivity.UNKNOWN: {
                if(confidence > 40){
                    statusTextView.setText("UNKNOWN");
                    break;
                }
            }
        }
    }


    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(DETECTED_ACTIVITY));
    }

    private void startService() {
        Intent intent = new Intent(MainActivity.this, RecognitionActivity.class);
        startService(intent);
    }

    private void stopService() {
        Intent intent = new Intent(MainActivity.this, RecognitionActivity.class);
        stopService(intent);
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onClick(View v) {

    }

    private void notification(String title, String content){
        NotificationCompat.Builder notification  = new NotificationCompat.Builder(this, CHANNEL_ID);
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        int notificationID = 1987;
        NotificationChannel channel;
        NotificationManager notificationManager;
        Intent intent;
        PendingIntent pendingIntent;
        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notification.setSmallIcon(R.drawable.cast_ic_stop_circle_filled_grey600).setContentTitle(title).setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT).setAutoCancel(true).setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channelName);
            String description = getString(R.string.channelDescription);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManagerCompat.notify(notificationID, notification.build());
    }



}