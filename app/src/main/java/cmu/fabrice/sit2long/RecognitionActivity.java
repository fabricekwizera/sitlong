package cmu.fabrice.sit2long;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

public class RecognitionActivity extends IntentService {
    private static final String TAG = "RecognitionActivity";
    private static final String CHANNEL_ID = "This channel";
    private static final String DETECTED_ACTIVITY = "detecteActivity";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RecognitionActivity(String name) {
        super(name);
    }

    public RecognitionActivity(){
        super("RecognitionActivity");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult recognitionResult = ActivityRecognitionResult.extractResult(intent);
            activityLikelyHappening(recognitionResult.getProbableActivities());
            for (DetectedActivity activity : recognitionResult.getProbableActivities()){
                Log.d("DETECTED ", activity.getType() + "");
            }
        }
    }

    public void activityLikelyHappening(List<DetectedActivity> activities) {
        activities.forEach(activity -> {
            if (activity.getType() == DetectedActivity.IN_VEHICLE) {
                Log.d(TAG, "The activity likely happening : INSIDE VEHICLE " + activity.getConfidence());
                //generateNotification("Activity likely happening ",  "Driving vehicle, sure at about " + activity.getConfidence() + " percent");
                sendActivity(activity);
            } else if (activity.getType() == DetectedActivity.ON_BICYCLE) {
                Log.d(TAG, "The activity likely happening : RIDING BICYCLE " + activity.getConfidence());
                //generateNotification("Activity likely happening ", "Riding bicycle, sure at about " + activity.getConfidence()  + " percent");
                sendActivity(activity);
            } else if (activity.getType() == DetectedActivity.ON_FOOT) {
                Log.d(TAG, "The activity likely happening : FOOT " + activity.getConfidence());
                //generateNotification("Activity likely happening ", "On foot, sure at about " + activity.getConfidence() + " percent");
                sendActivity(activity);
            } else if (activity.getType() == DetectedActivity.RUNNING) {
                Log.d(TAG, "The activity likely happening : RUNNING " + activity.getConfidence());
                //generateNotification("Activity likely happening ", "Running, sure at about " + activity.getConfidence() + " percent");
                sendActivity(activity);
            } else if (activity.getType() == DetectedActivity.STILL) {
                Log.d(TAG, "The activity likely happening : NOT MOVING " + activity.getConfidence());
                //generateNotification("Activity likely happening ", "Not moving, sure at about " + activity.getConfidence() + " percent");
                sendActivity(activity);
            } else if (activity.getType() == DetectedActivity.TILTING) {
                Log.d(TAG, "The activity likely happening : TILTING PHONE " + activity.getConfidence());
                //generateNotification("Activity likely happening ", "Tilting phone, sure at about " + activity.getConfidence() + " percent");
                sendActivity(activity);
            } else if (activity.getType() == DetectedActivity.WALKING) {
                Log.d(TAG, "The activity likely happening : WALKING " + activity.getConfidence());
                //generateNotification("Activity likely happening ", "Walking, sure at about " + activity.getConfidence() + " percent");
                sendActivity(activity);
            } else if (activity.getType() == DetectedActivity.UNKNOWN) {
                Log.d(TAG, "The activity likely happening : UNKNOWN " + activity.getConfidence());
                //generateNotification("Activity likely happening ", "Unknown, sure at about " + activity.getConfidence() + " percent");
                sendActivity(activity);
            }
        });
    }


    public void sendActivity(DetectedActivity activity){
        Intent intent = new Intent(DETECTED_ACTIVITY);
        intent.putExtra("Type", activity.getType());
        intent.putExtra("Confidence", activity.getConfidence());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
